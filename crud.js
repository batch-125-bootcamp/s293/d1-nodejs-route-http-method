let http = require("http");

const port = 3000;

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Robert",
		"email" : "robert@mail.com"
	}
];

http.createServer((req, res) => {

// retrieve the documents
	// url = /users
	// method = get
	
	if (req.url === "/users" && req.method === "GET"){
		
		res.writeHead(200, {"Content-Type" : "application/json"});
		
		res.write(JSON.stringify(directory));
		
		res.end();
	}

// create a new user
	// url = /users
	// method = post
	// data from the request will be coming from the client's body
		// req.body
	// req.on (on and on and on and on and on) The on method binds an event to a object.

	// It is a way to express your intent if there is something happening (data sent or error in your case) , then execute the function added as a parameter. This style of programming is called Event-driven programming. 

	if(req.url === "/users" && req.method === "POST"){

		let reqBody = "";
		req.on("data", (data) => {
			
			reqBody += data;		

		});

		req.on("end", () => {
			console.log(typeof reqBody);	/*string*/

			reqBody = JSON.parse(reqBody);
				/*parsed into a real JavaScript object*/
			console.log(reqBody);
				/*added documents*/

			let newUser = {
				"name" : reqBody.name,
				"email" : reqBody.email
			}
			directory.push(newUser); 	/*push or unshift to add*/
			console.log(directory);

			res.writeHead(200, 
			{"Content-Type": "application/json"}
			);
			res.write(JSON.stringify(directory));
			res.end();
		});

	}


}).listen(port); 
console.log(`Server is now connected to port ${port}`)