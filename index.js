let http = require("http");
const port = 3000;

// important for the server is the uri(endpoint) or resource and the http method
// part of the request is also the body

http.createServer((req, res) => {
	console.log(req);
	
	if (req.url === "/profile" && req.method === "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to my page!")

	} else if (req.url === "/register" && req.method === "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database.")
	
	} else {
	res.writeHead(404, {"Content-Type" : "text/plain"});
	res.end("Sorry! Page not found.")}
	
}).listen(port);

console.log(`Server is now connected to port ${port}`);